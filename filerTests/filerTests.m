//
//  filerTests.m
//  filerTests
//
//  Created by Owen Imholte on 7/23/15.
//  Copyright (c) 2015 swingingsultan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "SNDirectory.h"

@interface filerTests : XCTestCase

@property (nonatomic, retain) SNDirectory *testsBase;

@end

@implementation filerTests

//http://stackoverflow.com/a/6907432/1143123
- (SNDirectory *)testsDirectory {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString *testsPath = [NSString stringWithFormat:@"%@/tests",basePath];
    
    BOOL fileExistsAtPath = [[NSFileManager defaultManager] fileExistsAtPath:testsPath isDirectory:NULL];
    if(!fileExistsAtPath) {
        return [SNDirectory newDirectoryNamed:@"tests" atPath:basePath];
    } else {
        return [[SNDirectory alloc] initFile:@"tests" WithPath:basePath];
    }
}

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    self.testsBase = [self testsDirectory];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    [[NSFileManager defaultManager] removeItemAtPath:self.testsBase.fullpath error:nil];
}

- (void)testEmptyDir {
    SNDirectory *someDir = [SNDirectory newDirectoryNamed:@"hello" atPath:self.testsBase.fullpath];
    XCTAssertEqual(someDir.children.count,0, @"Expecting a new directory to have no items");
}

- (void)testSingleFile {
    SNDirectory *onefile = [SNDirectory newDirectoryNamed:@"onefile" atPath:self.testsBase.fullpath];
    [SNFile newFileNamed:@"the-file" atPath:onefile.fullpath];
    XCTAssertEqual(onefile.children.count,1, @"Expecting a new directory to have no items");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
