//
//  SNFile.m
//  filer
//
//  Created by Owen Imholte on 7/29/15.
//  Copyright (c) 2015 swingingsultan. All rights reserved.
//

#import "SNFile.h"

@interface SNFile ()

@property (nonatomic, retain) NSString *filepath;
@property (nonatomic, retain) NSString *filename;

@end

@implementation SNFile

-(id)initFile:(NSString*)name WithPath:(NSString*)path {
    self = [super init];
    if(self) {
        self.filename = name;
        self.filepath = path;
    }
    return self;
}

+(SNFile*)newFileNamed:(NSString*)filename atPath:(NSString*)fullPath {
    NSString *pathWithName = [NSString stringWithFormat:@"%@/%@",fullPath,filename];
    
    NSFileManager *fm = [NSFileManager defaultManager];
    BOOL isSuccess = [fm createFileAtPath:pathWithName contents:[NSData dataWithBytes:"" length:0] attributes:@{}];
    
    if (!isSuccess) {
        NSLog(@"Error creating file");
    }
    
    return [[SNFile alloc] initFile:filename WithPath:fullPath];
}

-(NSString*)fullpath {
    return [NSString stringWithFormat:@"%@/%@",self.filepath,self.filename];
}

@end
