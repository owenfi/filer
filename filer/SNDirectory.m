//
//  SNDirectory.m
//  filer
//
//  Created by Owen Imholte on 7/29/15.
//  Copyright (c) 2015 swingingsultan. All rights reserved.
//

#import "SNDirectory.h"

@interface SNDirectory () {
    NSArray *_children;
}

@property (nonatomic, retain) NSArray *children;

@end

@implementation SNDirectory

-(id)initFile:(NSString*)name WithPath:(NSString*)path {
    self = [super initFile:name WithPath:path];
    if(self) {
        _children = nil;
    }
    return self;
}

// This is more like an init syntactic sugar method
+(SNDirectory*)newDirectoryNamed:(NSString*)dirname atPath:(NSString*)fullPath {
    NSString *pathWithName = [NSString stringWithFormat:@"%@/%@",fullPath,dirname];
    
    NSFileManager *fm = [NSFileManager defaultManager];
    NSError *err;
    BOOL success = [fm createDirectoryAtPath:pathWithName withIntermediateDirectories:nil attributes:@{} error:&err];
    
    if (!success) {
        NSLog(@"Error creating directory: %@",err);
    }
    
    return [[SNDirectory alloc] initFile:dirname WithPath:fullPath];
}

-(NSArray*)children {
    // "Lazily" load children enables breadth first directory loading
    if (_children == nil) {
        [self reloadData];
    }

    return _children;
}

-(void)setChildren:(NSArray *)children {
    _children = children;
}

-(void)reloadData {
    NSError *err;
    NSArray *myContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:self.fullpath error:&err];
    
    NSMutableArray *subclassChildren = [[NSMutableArray alloc] init];
    BOOL isDirectory;
    for (NSString *name in myContents) {
        NSString *aFile = [NSString stringWithFormat:@"%@/%@",self.fullpath,name];
        
        BOOL fileExistsAtPath = [[NSFileManager defaultManager] fileExistsAtPath:aFile isDirectory:&isDirectory];
        if(fileExistsAtPath && isDirectory) {
            [subclassChildren addObject:[[SNDirectory alloc] initFile:name WithPath:self.fullpath]];
        } else if (fileExistsAtPath) {
            [subclassChildren addObject:[[SNFile alloc] initFile:name WithPath:self.fullpath]];
        } else {
            NSLog(@"No file");
        }
    }
    
    self.children = [subclassChildren copy];
}

@end
