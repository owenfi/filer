//
//  SNFile.h
//  filer
//
//  Created by Owen Imholte on 7/29/15.
//  Copyright (c) 2015 swingingsultan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SNFile : NSObject

-(id)initFile:(NSString*)name WithPath:(NSString*)path;

-(NSString*)filepath;
-(NSString*)filename;
-(NSString*)fullpath;

+(SNFile*)newFileNamed:(NSString*)filename atPath:(NSString*)fullPath;


@end
