//
//  SNFileCollectionViewController.h
//  filer
//
//  Created by Owen Imholte on 7/29/15.
//  Copyright (c) 2015 swingingsultan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SNDirectory.h"

@interface SNFileCollectionViewController : UICollectionViewController <UIActionSheetDelegate, UIAlertViewDelegate>

@property (atomic, retain) SNDirectory *topLevel;

-(IBAction)addNewFileOrDirectory:(id)sender;

@end
