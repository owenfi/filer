//
//  SNDirectory.h
//  filer
//
//  Created by Owen Imholte on 7/29/15.
//  Copyright (c) 2015 swingingsultan. All rights reserved.
//

#import "SNFile.h"

@interface SNDirectory : SNFile

// API to get children of a directory (files and dirs)
-(NSArray*)children;

+(SNDirectory*)newDirectoryNamed:(NSString*)dirname atPath:(NSString*)fullPath;

-(void)reloadData;

@end
