//
//  SNFileCollectionViewCell.h
//  filer
//
//  Created by Owen Imholte on 7/29/15.
//  Copyright (c) 2015 swingingsultan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SNFileCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UILabel *filename;

@end
