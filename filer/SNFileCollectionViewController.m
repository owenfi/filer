//
//  SNFileCollectionViewController.m
//  filer
//
//  Created by Owen Imholte on 7/29/15.
//  Copyright (c) 2015 swingingsultan. All rights reserved.
//

#import "SNFileCollectionViewController.h"
#import "SNFileCollectionViewCell.h"
#import "AppDelegate.h"
#import "SNFile.h"

@interface SNFileCollectionViewController ()

@end

@implementation SNFileCollectionViewController

static NSString * const fileReuseIdentifier = @"FileCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(self == [self.navigationController.viewControllers objectAtIndex:0]) {
        self.topLevel = [[SNDirectory alloc] initFile:@"root" WithPath:[AppDelegate applicationDocumentsDirectory]];
        self.navigationItem.title = @"Top Level";
    } else {
        self.navigationItem.title = self.topLevel.filename;
    }
    
    // Register cell classes
    UINib *fileNib = [UINib nibWithNibName:@"SNFileCollectionViewCell" bundle:[NSBundle mainBundle]];
    [self.collectionView registerNib:fileNib forCellWithReuseIdentifier:fileReuseIdentifier];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSArray *arr = [self.topLevel children];
    return arr.count;
}

- (SNFile*)fileForIndexPath:(NSIndexPath*)indexPath {
    return self.topLevel.children[indexPath.row];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SNFileCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:fileReuseIdentifier forIndexPath:indexPath];
    
    SNFile *f = [self fileForIndexPath:indexPath];
    
    cell.backgroundColor = [UIColor whiteColor];
    if([f isKindOfClass:[SNDirectory class]]) {
        cell.backgroundColor = [UIColor colorWithRed:.8 green:.8 blue:.95 alpha:1.0];
    }
    cell.filename.text = f.filename;
    return cell;
}

-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    SNFile *f = [self fileForIndexPath:indexPath];
    if([f isKindOfClass:[SNDirectory class]]) {
        return YES;
    }
    
    return NO;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    SNFileCollectionViewController *fc = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"FileCollectionController"];
    fc.topLevel = (SNDirectory*)[self fileForIndexPath:indexPath];
    [self.navigationController pushViewController:fc animated:YES];
}

//// Do display code here as subviews are now loaded
//
// Scratch that - just needed to put the cell in a nib as I can't figure out reuse from storyboard prototype
//
//-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
//    SNFileCollectionViewCell *fileCell = (SNFileCollectionViewCell*)cell;
//    
//    // Configure the cell
//    fileCell.filename.text = @"Some file";
//    
////    cell.backgroundColor = [UIColor whiteColor];
//}

#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

#pragma mark - file creation methods

#define kMakeFile @"File"
#define kMakeDirectory @"Directory"
#define kMultiFile @"100 Files"
#define kMultiDirectory @"100 Folders"

-(IBAction)addNewFileOrDirectory:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"What to create?" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:kMakeFile, kMakeDirectory, kMultiFile, kMultiDirectory, nil];
    UIBarButtonItem *bb = self.navigationItem.rightBarButtonItem;
    [actionSheet showFromBarButtonItem:bb animated:YES];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {

    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    if ([buttonTitle isEqualToString:kMakeFile]) {
        [self showAlertViewNamePrompt:NO];
    } else if ([buttonTitle isEqualToString:kMakeDirectory]) {
        [self showAlertViewNamePrompt:YES];
    } else if ([buttonTitle isEqualToString:kMultiFile]) {
        for (int i = 0; i < 100; i++) {
            [SNFile newFileNamed:[[NSUUID UUID] UUIDString] atPath:self.topLevel.fullpath];
            [self refresh];
        }
    } else if ([buttonTitle isEqualToString:kMultiDirectory]) {
        for (int i = 0; i < 100; i++) {
            [SNDirectory newDirectoryNamed:[[NSUUID UUID] UUIDString] atPath:self.topLevel.fullpath];
            [self refresh];
        }
    } else {
        NSLog(@"Don't know what to do here");
    }
}

-(void)showAlertViewNamePrompt:(BOOL)isDirectory {
    NSString *title = @"Enter a filename";
    if(isDirectory) {
        title = @"Enter a directory name";
    }
    
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:title message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Okay", nil];
    av.alertViewStyle = UIAlertViewStylePlainTextInput;
    [av show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *input = [alertView textFieldAtIndex:0].text;

    if ([alertView.title containsString:@"filename"]) {
        [SNFile newFileNamed:input atPath:self.topLevel.fullpath];
    } else if ([alertView.title containsString:@"directory"]) {
        [SNDirectory newDirectoryNamed:input atPath:self.topLevel.fullpath];
    }
    
    [self refresh];
}

-(void)refresh {
    [self.topLevel reloadData];
    [self.collectionView reloadData];
}

@end
